Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: krunner
Source: https://invent.kde.org/frameworks/krunner
Upstream-Contact: kde-frameworks-devel@kde.org

Files: *
Copyright: 2006-2011, Aaron Seigo <aseigo@kde.org>
           2020-2023 Alexander Lohnau <alexander.lohnau@gmx.de>
           2017 David Edmundson <davidedmundson@kde.org>
           2008, Jordi Polo <mumismo@gmail.com>
           2020 Kai Uwe Broulik <kde@broulik.de>
           2011, Marco Martin <mart@kde.org>
           2007-2009, Ryan P. Bitanga <ryan.bitanga@gmail.com>
           2014, Vishesh Handa <vhanda@kde.org>
License: LGPL-2+

Files: autotests/runnermanagerhistorytest.cpp
       autotests/runnermanagersinglerunnermodetest.cpp
       autotests/runnermanagertest.cpp
       autotests/testmetadataconversion.cpp
       autotests/threadingtest.cpp
       templates/runner6/src/%{APPNAMELC}.cpp
       templates/runner6/src/%{APPNAMELC}.h
Copyright: 2021-2023 Alexander Lohnau <alexander.lohnau@gmx.de>
           2022 Eduardo Cruz <eduardo.cruz@kdemail.net>
License: LGPL-2.1+

Files: src/model/*
Copyright: 2019 Kai Uwe Broulik <kde@broulik.de>
           2023 Alexander Lohnau <alexander.lohnau@gmx.de>
License: LGPL-3+KDEeV

Files: KF6KRunnerMacros.cmake
       KF6RunnerConfig.cmake.in
       src/CMakeLists.txt
       templates/CMakeLists.txt
       CMakeLists.txt
Copyright: 2020 Alexander Lohnau <alexander.lohnau@gmx.de>
           Aleix Pol <aleixpol@kde.org>
           Friedrich W. H. Kossebau <kossebau@kde.org>
License: BSD-2-clause

FIles: autotests/plugins/metadatafile1.json
       docs/Doxyfile.local
       ExtraDesktop.sh
       .git-blame-ignore-revs
       .gitignore
       .gitlab-ci.yml
       .kde-ci.yml
       metainfo.yaml
       README.md
       templates/.clang-format
Copyright: 2021 Alexander Lohnau <alexander.lohnau@gmx.de>
           2020 Volker Krause <vkrause@kde.org>
License: CC0-1.0

Files: debian/*
Copyright: 2014, Scarlett Clark <scarlett@scarlettgatelyclark.com>
           2024 Patrick Franz <deltaone@debian.org>
License: LGPL-2+

License: LGPL-2+
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Library General Public
 License as published by the Free Software Foundation; either
 version 2 of the License, or (at your option) any later version.
 .
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Library General Public License for more details.
 .
 You should have received a copy of the GNU Library General Public License
 along with this library; see the file COPYING.LIB.  If not, write to
 the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 Boston, MA 02110-1301, USA.
 .
 On Debian systems, the complete text of the GNU Library General
 Public License version 2 can be found in "/usr/share/common-licenses/LGPL-2".

License: LGPL-2.1+
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.
 .
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 .
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 .
 On Debian systems, the complete text of the GNU Lesser General Public License
 version 2.1 can be found in "/usr/share/common-licenses/LGPL-2.1".

License: LGPL-3+KDEeV
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 3 of the license or (at your option) any later version
 that is accepted by the membership of KDE e.V. (or its successor
 approved by the membership of KDE e.V.), which shall act as a
 proxy as defined in Section 6 of version 3 of the license.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.
 .
 On Debian systems, the complete text of the GNU Lesser General Public License
 version 3 can be found in "/usr/share/common-licenses/LGPL-3".

License: CC0-1.0
 CREATIVE COMMONS CORPORATION IS NOT A LAW FIRM AND DOES NOT PROVIDE LEGAL
 SERVICES. DISTRIBUTION OF THIS DOCUMENT DOES NOT CREATE AN ATTORNEY-CLIENT
 RELATIONSHIP. CREATIVE COMMONS PROVIDES THIS INFORMATION ON AN "AS-IS" BASIS.
 CREATIVE COMMONS MAKES NO WARRANTIES REGARDING THE USE OF THIS DOCUMENT OR
 THE INFORMATION OR WORKS PROVIDED HEREUNDER, AND DISCLAIMS LIABILITY FOR
 DAMAGES RESULTING FROM THE USE OF THIS DOCUMENT OR THE INFORMATION OR WORKS
 PROVIDED HEREUNDER.
 .
 Statement of Purpose
 .
 The laws of most jurisdictions throughout the world automatically confer
 exclusive Copyright and Related Rights (defined below) upon the creator and
 subsequent owner(s) (each and all, an "owner") of an original work of
 authorship and/or a database (each, a "Work").
 .
 On Debian systems, the complete text of the Creative Commons Zero v1.0 Universal
 license can be found in "/usr/share/common-licenses/CC0-1.0".

License: BSD-2-clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 .
 1. Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.
 2. Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in the
    documentation and/or other materials provided with the distribution.
 .
 THIS SOFTWARE IS PROVIDED BY APPLE, INC. ``AS IS'' AND ANY
 EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL APPLE, INC. OR
 CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
